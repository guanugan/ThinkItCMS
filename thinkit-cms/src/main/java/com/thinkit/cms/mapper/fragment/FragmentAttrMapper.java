package com.thinkit.cms.mapper.fragment;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.fragment.FragmentAttrDto;
import com.thinkit.cms.entity.fragment.FragmentAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 页面片段数据表 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-03
 */
@Mapper
public interface FragmentAttrMapper extends BaseMapper<FragmentAttr> {

    IPage<FragmentAttr> listPage(IPage<FragmentAttr> pages, @Param("dto") FragmentAttrDto dto);

    List<Map> listDataByCode(@Param("code") String code);
}
