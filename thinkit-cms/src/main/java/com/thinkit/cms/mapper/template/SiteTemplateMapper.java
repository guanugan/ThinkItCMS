package com.thinkit.cms.mapper.template;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.site.SiteTemplateDto;
import com.thinkit.cms.entity.site.SiteTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 站点模型表 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Mapper
public interface SiteTemplateMapper extends BaseMapper<SiteTemplate> {

    SiteTemplateDto ckInstall(@Param("siteId") String siteId, @Param("templateId") String templateId);

    void unstallAll(@Param("siteId") String siteId);

    String findTemplatePath(@Param("root") String root,@Param("siteId") String siteId);

    void updateTempLocation(@Param("templateId") String templateId, @Param("loaction") String loaction);

    SiteTemplateDto findTemplate(@Param("root") String root, @Param("siteId") String siteId);

    String findTemplateId (@Param("siteId") String siteId);
}
