package com.thinkit.cms.mapper.admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.admin.ApiClient;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-05-05
 */
@Mapper
public interface ApiClientMapper extends BaseMapper<ApiClient> {

    List<String> selectResources(@Param("clientId") String clientId);
}
