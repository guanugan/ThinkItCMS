package com.thinkit.cms.mapper.model;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.model.Model;
import com.thinkit.utils.model.KeyValueModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2019-10-23
 */
@Mapper
public interface ModelMapper extends BaseMapper<Model> {

    List<Model> listModelByCategoryId(@Param("siteId") String siteId,
                                      @Param("categoryId") String categoryId,
                                      @Param("templateId") String templateId);

    List<KeyValueModel> listPublihModel(@Param("categoryId") String categoryId,
                                        @Param("siteId") String siteId,
                                        @Param("templateId") String templateId);


    String getFormDesign(@Param("modelId") String modelId);

    Boolean getFormHasfile(@Param("modelId") String modelId);

    Model getModel(@Param("modelId")  String modelId);
}
