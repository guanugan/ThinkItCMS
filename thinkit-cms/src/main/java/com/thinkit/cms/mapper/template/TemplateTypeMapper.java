package com.thinkit.cms.mapper.template;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.template.TemplateTypeDto;
import com.thinkit.cms.entity.template.TemplateType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 模板分类 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-07-28
 */
@Mapper
public interface TemplateTypeMapper extends BaseMapper<TemplateType> {


    List<TemplateTypeDto> listByPid(@Param("pid") String pid);

    TemplateTypeDto getById(@Param("id") String id);

    Integer ckHasCode(@Param("id") String id,@Param("code") String code);

    void deleteByPid(@Param("pid") String pid);

    TemplateTypeDto getByCode(@Param("code") String code);
}
