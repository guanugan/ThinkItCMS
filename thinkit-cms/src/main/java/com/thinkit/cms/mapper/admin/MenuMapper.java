package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.admin.MenuDto;
import com.thinkit.cms.entity.admin.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author dl
 * @since 2018-03-21
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

	List<MenuDto> selectMenuTreeByUid(@Param("userId") String userId);

	List<String> selectPermsByUid(@Param("userId") String userId, @Param("application") String application);

	List<MenuDto> selectMenuUid(@Param("userId") String userId);

	List<String> selectPermsByUrl(@Param("url") String url);

	List<MenuDto> loadMenu(@Param("userId") String userId);

    Integer ckHasPerm(@Param("perm") String perm, @Param("id") String id);

    void hideIt(@Param("id") String id, @Param("hidden") Integer hidden);

    List<MenuDto> selectTreeList();
}
