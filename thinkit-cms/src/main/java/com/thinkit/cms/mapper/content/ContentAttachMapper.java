package com.thinkit.cms.mapper.content;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.content.ContentAttach;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容附件 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-19
 */
@Mapper
public interface ContentAttachMapper extends BaseMapper<ContentAttach> {

    List<String> listData(@Param("contentId") String contentId);

    List<Map> listAttach(@Param("contentId") String contentId);

    List<Map> listAttachs(@Param("contentIds") List<String> contentIds);
}
