package com.thinkit.cms.mapper.resource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.resource.SysResourceDto;
import com.thinkit.cms.entity.resource.SysResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2019-11-11
 */
@Mapper
public interface SysResourceMapper extends BaseMapper<SysResource> {

    IPage<SysResourceDto> listGroupPage(IPage<SysResourceDto> pages, @Param("dto") SysResourceDto dto,
                                        @Param("gids") List<String> gids, @Param("siteId") String siteId);

    String getFileUrl(@Param("id") String id, @Param("siteId") String siteId);
}
