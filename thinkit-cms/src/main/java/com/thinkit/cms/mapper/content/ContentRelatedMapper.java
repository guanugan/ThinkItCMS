package com.thinkit.cms.mapper.content;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.entity.content.ContentRelated;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 内容推荐 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-09-30
 */
@Mapper
public interface ContentRelatedMapper extends BaseMapper<ContentRelated> {

    List<ContentDto> listRelated(@Param("contentId") String contentId);
}
