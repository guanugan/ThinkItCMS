package com.thinkit.cms.mapper.model;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.model.ModelTemplateDto;
import com.thinkit.cms.entity.model.ModelTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 模型模板针对不同的站点不同的模型下有不同的 模板页面 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Mapper
public interface ModelTemplateMapper extends BaseMapper<ModelTemplate> {

    ModelTemplateDto getModelTemplate(@Param("modelId") String modelId, @Param("templateId") String templateId, @Param("siteId") String siteId);
}
