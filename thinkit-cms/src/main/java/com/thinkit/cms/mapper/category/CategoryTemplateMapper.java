package com.thinkit.cms.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.category.CategoryTemplateDto;
import com.thinkit.cms.entity.category.CategoryTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 分类-模板配置表 Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-08-12
 */
@Mapper
public interface CategoryTemplateMapper extends BaseMapper<CategoryTemplate> {

    void updateTemplate(@Param("categoryId") String categoryId,
                        @Param("siteId") String siteId,
                        @Param("templateId") String templateId,
                        @Param("templatePath") String templatePath);

    String getCategoryTempPath(@Param("categoryId") String categoryId,
                               @Param("siteId") String siteId,
                               @Param("templateId") String templateId);

    CategoryTemplateDto getCategoryTemp(@Param("categoryId") String categoryId,
                                        @Param("siteId") String siteId,
                                        @Param("templateId") String templateId);

    String getTempPathByCategoryId(@Param("categoryId") String categoryId,@Param("siteId") String siteId);
}
