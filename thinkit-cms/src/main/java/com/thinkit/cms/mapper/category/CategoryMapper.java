package com.thinkit.cms.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.category.CategoryNavbar;
import com.thinkit.cms.entity.category.Category;
import com.thinkit.utils.model.Tree;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 分类 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-07
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

    List<Tree<CategoryDto>> treeCategory(@Param("siteId") String siteId);

    void deleteCategoryModel(@Param("categoryModelId") String categoryModelId);

    IPage<CategoryDto> listPage(IPage<CategoryDto> pages, @Param("dto") CategoryDto dto);

    Integer checkMaxCode(@Param("siteId") String siteId,@Param("code") String code, @Param("id") String id);

    CategoryDto getDetail(@Param("id") String id);

    Map<String,Object> getMapDetail(@Param("id") String id,@Param("code") String code,@Param("siteId") String siteId);

    String getCategoryTempPath(@Param("categoryId") String id, @Param("siteId") String siteId);

    List<CategoryDto> listCategoryByPid(@Param("categoryId") String categoryId);

    List<CategoryDto> treeCategoryForContent(@Param("siteId") String siteId);

    String getCode(@Param("categoryId") String categoryId);

    List<CategoryNavbar> navbar(@Param("siteId") String siteId);

    List<Map<String, Object>> getMapDetails(@Param("id") String id,@Param("siteId") String siteId);

    List<String> getIds(@Param("siteId") String siteId);

    Integer queryHasContent(@Param("categoryId") String categoryId);

    List<String> getContentIds(@Param("categoryId") String categoryId);

    Integer queryPageSize(@Param("code") String code, @Param("categoryId") String categoryId);
}
