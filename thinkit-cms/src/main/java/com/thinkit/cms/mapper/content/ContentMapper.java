package com.thinkit.cms.mapper.content;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.content.ContentAnalysisDto;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.directives.directive.ListDirectiveModel;
import com.thinkit.cms.entity.content.Content;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Mapper
public interface ContentMapper extends BaseMapper<Content> {

    IPage<ContentDto> listPage(IPage<ContentDto> pages, @Param("dto") ContentDto dto);

    void updateStatus(@Param("id") String id,@Param("status") String status);

    Map<String, Object> loadTempParams(@Param("contentId") String contentId, @Param("statues") List<String> status);

    List<Map<String, Object>> load1000Params(@Param("contentId") String contentId, @Param("statues") List<String> status);

    Map<String, Object> categorySingleData(@Param("categoryId") String contentId, @Param("code")String code,@Param("siteId") String siteId);

    void top(@Param("dto") ContentDto contentDto);

    List<String> getTopTag(@Param("siteId") String siteId);

    Integer publish(@Param("ids") List<String> ids,
                    @Param("status") String status,
                    @Param("date") Date date,
                    @Param("byJob") Boolean byJob);

    String getPathRule(@Param("id") String id);

    List<Map<String, Object>> listContent(@Param("dto") ListDirectiveModel directiveModel);

    IPage<Map<String, Object>> pageContent(IPage<Map<String, Object>> pages,
                                           @Param("categoryId") String categoryId,
                                           @Param("siteId") String siteId);

    List<Map<String, Object>> listByCode(@Param("siteId") String siteId,
                                         @Param("code") String code,
                                         @Param("categoryId") String categoryId,
                                         @Param("num") Integer num,
                                         @Param("ascFields") List<String> asc,
                                         @Param("descFields") List<String> desc);

    Map<String, Object> nextPrevious(@Param("id") String id, @Param("next") Boolean next,@Param("categoryId") String categoryId);

    IPage<Map<String, String>> pageAllContentForGen(IPage<Map<String, String>> pages, @Param("siteId") String siteId);

    List<String> listCategoryByCids(@Param("ids") List<String> ids);

    String getCategoryId(@Param("id") String id);

    List<Map<String, Object>> getTopNews(@Param("categoryId") String categoryId,
                                       @Param("codes") List<String> codes,
                                       @Param("siteId") String siteId,
                                       @Param("field") String field,
                                       @Param("num") Integer num);


    List<Map<String, List>> getTopTags(@Param("categoryId") String categoryId,
                                       @Param("codes") List<String> codes,
                                       @Param("siteId") String siteId,
                                       @Param("tag") String tag,
                                       @Param("num") Integer num);


    List<Map<String, Object>> buildParams(@Param("ids") List<String> ids);

    IPage<ContentDto> pageRecycler(IPage<ContentDto> pages, @Param("dto") ContentDto dto);

    String findUrl(@Param("id") String id);

    void updateCategoryId(@Param("categoryId") String categoryId,@Param("contentIds") List<String> contentIds);

    void jobPublish(@Param("contentIds") List<String> ids, @Param("date") Date date);

    ContentDto getInfo(@Param("contentId") String contentId);

    Long viewTimes(@Param("contentId") String contentId);

    Long viewLikes(@Param("contentId") String contentId);

    List<ContentAnalysisDto> analysisMonthTop(@Param("siteId") String siteId, @Param("status") Integer status, @Param("year") Integer year);

    List<ContentAnalysisDto> analysisCategoryTop(@Param("siteId") String siteId);

    IPage<ContentDto> pageContentForNoSql(IPage<Map<String, String>> pages, @Param("siteId") String siteId );

    void updateViewLikes(@Param("field") String field,@Param("value") Integer value,@Param("id") String id);

    List<ContentDto> listContents(@Param("contentIds") List<String> contentIds);

    List<Map<String, Object>> listUpToDate(@Param("siteId")  String siteId,
                                           @Param("code") String code,
                                           @Param("categoryId")  String categoryId,
                                           @Param("num")  Integer num,
                                           @Param("ascFields") List<String> asc,
                                           @Param("descFields") List<String> desc,
                                           @Param("publish") Boolean publish);
}
