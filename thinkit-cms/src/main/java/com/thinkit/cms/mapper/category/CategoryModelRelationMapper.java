package com.thinkit.cms.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.category.CategoryModelRelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 分类选择模型时关系表 该模型值 tk_model 表 不是 分类模型表 Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-08-10
 */
@Mapper
public interface CategoryModelRelationMapper extends BaseMapper<CategoryModelRelation> {

    void deleteByCSId(@Param("siteId") String siteId,
                      @Param("categoryId")  String categoryId,
                      @Param("templateId") String templateId);

    String loadTemplatePath(@Param("params") Map<String, Object> params);

    String getTempPathByCategoryId(@Param("categoryId") String categoryId,
                                   @Param("modelId") String modelId,
                                   @Param("siteId")String siteId);
}
