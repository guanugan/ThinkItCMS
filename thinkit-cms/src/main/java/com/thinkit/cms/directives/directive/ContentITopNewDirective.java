package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 获取指定/不指定 栏目 是否置顶  是否推荐 是否头条
 */
@Component
public class ContentITopNewDirective extends BaseDirective {

    @Autowired
    ContentService contentService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String categoryId=render.getString("categoryId");// 分类id
        String code=render.getString("code"); // 分类编码
        String where=render.getString("where"); // 分类编码
        if(Checker.BeNotBlank(where)){
            Map<String,List> maps = contentService.getTopNews(categoryId,code,where);
            render.putAll(maps).render();
        }
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.CMS_TOPNEW_DIRECTIVE;
    }
}
