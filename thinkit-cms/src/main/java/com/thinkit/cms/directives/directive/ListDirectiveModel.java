package com.thinkit.cms.directives.directive;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
@Slf4j
public class ListDirectiveModel {
    
    private BaserRender handler;
    private String siteId;
    private List<String> asc = new ArrayList<>();
    private List<String> desc = new ArrayList<>();
    private String categoryId ;
    private String categoryCode ;
    private Boolean isPage = true;
    private Map<String,Object> page ;

    public ListDirectiveModel(BaserRender renderHandler){
        this.handler = renderHandler;
        try {
            initParams();
        }catch (Exception e){
            log.error("参数初始化失败："+e.getMessage());
        }
    }

    private void initParams() throws Exception {
        String asc = handler.getString("asc");
        String desc = handler.getString("desc");
        this.categoryId = handler.getString("categoryId");
        this.categoryCode = handler.getString("categoryCode");
        Object pageObj = null;// handler.getAttribute("page");
        if(Checker.BeNotNull(pageObj)){
            this.page = (Map<String,Object>) pageObj;
            this.isPage = (Boolean) page.get("allowContribute") && (Integer) page.get("pageSize")>0;
        }
        if(Checker.BeNotBlank(asc)){
            this.asc = Arrays.asList(asc.split(","));
        }
        if(Checker.BeNotBlank(desc)){
            this.desc = Arrays.asList(desc.split(","));
        }
    }
}
