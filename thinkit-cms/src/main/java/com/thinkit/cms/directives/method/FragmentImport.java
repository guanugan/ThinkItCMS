package com.thinkit.cms.directives.method;

import com.thinkit.cms.api.fragment.FragmentModelService;
import com.thinkit.directive.emums.LangEnum;
import com.thinkit.directive.emums.MethodEnum;
import com.thinkit.directive.render.BaseMethod;
import com.thinkit.utils.utils.Checker;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FragmentImport extends BaseMethod {

    @Autowired
    FragmentModelService fragmentModelService;

    @Override
    public Object exec(List arguments) throws TemplateModelException {
        String code = getString(0, arguments, LangEnum.STRING);
        if (Checker.BeNotBlank(code)) {
            String fragmentPath = fragmentModelService.getFragmentPathByCode(code);
            if (Checker.BeNotBlank(fragmentPath)) {
                return fragmentPath;
            }
        }
        return null;
    }

    @Override
    public MethodEnum getName() {
        return MethodEnum.IMPORT;
    }
}
