package com.thinkit.cms.service.content;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.content.ContentRelatedService;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.ContentRelatedDto;
import com.thinkit.cms.entity.content.ContentRelated;
import com.thinkit.cms.mapper.content.ContentRelatedMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 内容推荐 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-09-30
 */
@Transactional
@Service
public class ContentRelatedServiceImpl extends BaseServiceImpl<ContentRelatedDto, ContentRelated, ContentRelatedMapper> implements ContentRelatedService {


    @Override
    public void saveRelatedBath(List<ContentRelatedDto> dtos) {
        if(Checker.BeNotEmpty(dtos)){
            List<ContentRelatedDto> hasDtos=listByField("content_id",dtos.get(0).getContentId());
            if(Checker.BeNotEmpty(hasDtos)){
                List<String> relatedids = new ArrayList<>();// 已经推荐过得文章id
                for(ContentRelatedDto contentRelatedDto:hasDtos){
                    relatedids.add(contentRelatedDto.getRelatedCid());
                }
                for(ContentRelatedDto dto:dtos){
                    if(!relatedids.contains(dto.getRelatedCid())){
                        insert(dto);
                    }
                }
            }else{
                super.insertBatch(dtos);
            }
        }
    }

    @Override
    public List<ContentDto> listRelated(String contentId) {
         List<ContentDto> contents=baseMapper.listRelated(contentId);
        return Checker.BeNotEmpty(contents) ? contents: Lists.newArrayList();
    }
}
