package com.thinkit.cms.service.job.handler;

import com.thinkit.cms.dto.content.PublishDto;
import com.thinkit.processor.job.JobExecuteHandler;
import com.thinkit.utils.utils.Checker;
import org.quartz.Scheduler;
import org.quartz.Trigger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PublishHandler extends JobExecuteHandler {
    @Override
    public boolean before(Scheduler scheduler, Trigger trigger, Map<String, Object> param) {
        PublishDto publishDto = (PublishDto)trigger.getJobDataMap().get("bean");
        PublishDto newBean = (PublishDto)param.get("bean");
        List<String> ids = publishDto.getIds();
        if(Checker.BeEmpty(ids)){
            ids = new ArrayList<>();
        }
        if(Checker.BeNotEmpty(newBean.getIds())){
            for (String id : newBean.getIds()) {
                if (!ids.contains(id)) {
                    ids.add(id);
                }
            }
        }
        trigger.getTriggerBuilder().startAt(newBean.getDate());
        newBean.setIds(ids);
        return true;
    }

    @Override
    public void after(Scheduler scheduler, Trigger trigger, Map<String, Object> param) {

    }
}
