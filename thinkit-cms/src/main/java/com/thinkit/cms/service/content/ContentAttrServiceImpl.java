package com.thinkit.cms.service.content;
import com.thinkit.cms.api.content.ContentAttrService;
import com.thinkit.cms.dto.content.ContentAttrDto;
import com.thinkit.cms.entity.content.ContentAttr;
import com.thinkit.cms.mapper.content.ContentAttrMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 内容扩展 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Transactional
@Service
public class ContentAttrServiceImpl extends BaseServiceImpl<ContentAttrDto, ContentAttr, ContentAttrMapper> implements ContentAttrService {



}
