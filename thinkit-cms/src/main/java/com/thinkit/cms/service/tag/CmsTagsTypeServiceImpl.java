package com.thinkit.cms.service.tag;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.tag.CmsTagsTypeService;
import com.thinkit.cms.api.tag.TagService;
import com.thinkit.cms.dto.tag.CmsTagsTypeDto;
import com.thinkit.cms.dto.tag.TagDto;
import com.thinkit.cms.entity.tag.CmsTagsType;
import com.thinkit.cms.mapper.tag.CmsTagsTypeMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.core.constant.Constants;
import com.thinkit.core.handler.CustomException;
import com.thinkit.nosql.annotation.CacheClear;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标签类型 服务实现类
 * </p>
 *
 * @author LG
 * @since 2020-01-31
 */
@Transactional
@Service
public class CmsTagsTypeServiceImpl extends BaseServiceImpl<CmsTagsTypeDto, CmsTagsType, CmsTagsTypeMapper> implements CmsTagsTypeService {

    @Autowired
    TagService tagService;


    @Override
    public PageDto<CmsTagsTypeDto> listPage(PageDto<CmsTagsTypeDto> pageDto) {
        IPage<CmsTagsTypeDto> pages = new Page<>(pageDto.getPageNo(), pageDto.getPageSize());
        IPage<CmsTagsTypeDto> result = baseMapper.listPage(pages, pageDto.getDto());
        PageDto<CmsTagsTypeDto> resultSearch = new PageDto(result.getTotal(), result.getPages(), result.getCurrent(), Checker.BeNotEmpty(result.getRecords()) ? result.getRecords() : Lists.newArrayList());
        return resultSearch;
    }


    @Override
    @CacheEvict(value = Constants.cacheName, key = "#root.targetClass+'.getByPk.'+#root.args[0]")
    public boolean deleteById(String id) {
        List<TagDto> cmsTagsDtos=tagService.listByField("type_id",id);
        if(Checker.BeNotEmpty(cmsTagsDtos)){
            throw  new CustomException(ApiResult.result(20023));
        }
        return super.deleteByPk(id);
    }

    @CacheClear(keys = {"getByPk"},clas = {TagServiceImpl.class})
    @Override
    public void tagsBelong(CmsTagsTypeDto v) {
        boolean cklegal=Checker.BeNotBlank(v.getId())&&Checker.BeNotEmpty(v.getTagIds());
        if(cklegal){
            List<TagDto> cmsTagsDtos=tagService.getByPks(v.getTagIds());
            if(Checker.BeNotEmpty(cmsTagsDtos)){
                cmsTagsDtos.forEach(cmsTagsDto->{
                    cmsTagsDto.setTypeId(v.getId());
                });
                tagService.updateByPks(cmsTagsDtos);
            }
        }
    }

    @Override
    public void save(CmsTagsTypeDto v) {
        String name=v.getName();
        boolean ckNotlegal = Checker.BeBlank(name)||Checker.BeNotEmpty(isExist(name));
        if(ckNotlegal){
            throw new CustomException(ApiResult.result(20024));
        }
        v.setName(name.toUpperCase().replaceAll("\\s*", ""));
        super.insert(v);
    }

    @Override
    @CacheEvict(value = Constants.cacheName, key = "#root.targetClass+'.getByPk.'+#p0.id")
    public void update(CmsTagsTypeDto v) {
        List<CmsTagsType> cmsTagsTypes=isExist(v.getName());
        if(Checker.BeNotEmpty(cmsTagsTypes)){
            if(!v.getId().equals(cmsTagsTypes.get(0).getId())){
                throw new CustomException(ApiResult.result(20024));
            }
        }
        v.setName(v.getName().toUpperCase().replaceAll("\\s*", ""));
        super.updateByPk(v);
    }

    private List<CmsTagsType> isExist(String name){
        Map<String,Object> map = new HashMap<>(16);
        map.put("name",name.toUpperCase().replaceAll("\\s*", ""));
        map.put("site_id",getSiteId());
        List<CmsTagsType> cmsTagsTypes=baseMapper.selectByMap(map);
        return cmsTagsTypes;
    }
}
