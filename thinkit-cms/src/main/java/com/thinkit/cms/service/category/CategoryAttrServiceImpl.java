package com.thinkit.cms.service.category;

import com.thinkit.cms.api.category.CategoryAttrService;
import com.thinkit.cms.dto.category.CategoryAttrDto;
import com.thinkit.cms.entity.category.CategoryAttr;
import com.thinkit.cms.mapper.category.CategoryAttrMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 分类扩展 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-10
 */
@Transactional
@Service
public class CategoryAttrServiceImpl extends BaseServiceImpl<CategoryAttrDto, CategoryAttr, CategoryAttrMapper> implements CategoryAttrService {


    @Override
    public void updateSeo(CategoryAttrDto v) {
        baseMapper.updateSeoByCategoryId(v.getCategoryId(),v.getTitle(),v.getKeywords(),v.getDescription());
    }

    @Override
    public void updateByCategory(String categoryId, String attrData) {
        baseMapper.updateByCategory(categoryId,attrData);
    }

    @Override
    public CategoryAttrDto getSeo(String categoryId) {
        return baseMapper.getSeo(categoryId);
    }
}
