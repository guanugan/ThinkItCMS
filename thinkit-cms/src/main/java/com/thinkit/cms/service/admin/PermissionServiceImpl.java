package com.thinkit.cms.service.admin;

import com.thinkit.cms.api.admin.PermissionService;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
@Service
public class PermissionServiceImpl extends PermissionLoader implements PermissionService {

    @Override
    public String selectPermIdsByUrl(String url, String clientId) {
        Map<String,String> map = loadPermsByUrl(clientId);
        return getPerm(map,url);
    }

    @Override
    public Set<String> selectPermsByUid(String userId,  String clientId) {
        return loadPermsByUid(userId,clientId);
    }
}
