package com.thinkit.cms.service.admin;

import com.thinkit.cms.api.admin.MemberService;
import com.thinkit.cms.dto.admin.MemberDto;
import com.thinkit.cms.entity.admin.Member;
import com.thinkit.cms.mapper.admin.MemberMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-11-03
 */
@Transactional
@Service
public class MemberServiceImpl extends BaseServiceImpl<MemberDto, Member, MemberMapper> implements MemberService {


    @Override
    public MemberDto loadMemUserByUsername(String account) {
        return baseMapper.loadMemUserByUsername(account);
    }
}
