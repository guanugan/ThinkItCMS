package com.thinkit.cms.controller.job;

import com.thinkit.cms.api.job.JobService;
import com.thinkit.cms.dto.content.PublishDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("job")
public class JobController {

    @Autowired
    JobService jobService;

    /**
     * 定时发布
     * @param publishDto
     */
    @PutMapping(value = "jobPublish")
    public void jobPublish(@Validated @RequestBody PublishDto publishDto){
        jobService.jobPublish(publishDto.setStatus("1"));
    }
}
