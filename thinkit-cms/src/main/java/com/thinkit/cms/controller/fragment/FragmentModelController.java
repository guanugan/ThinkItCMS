package com.thinkit.cms.controller.fragment;

import com.thinkit.cms.api.fragment.FragmentModelService;
import com.thinkit.cms.dto.fragment.FragmentModelDto;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.ApiResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * <p>
 * 页面片段文件模型 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-02
 */
@Validated
@RestController
@RequestMapping("fragmentModel")
public class FragmentModelController extends BaseController<FragmentModelService> {


    @GetMapping("getFragmentInfo")
    public FragmentModelDto getFragmentInfo(@NotBlank @RequestParam String fileName){
        return  service.getFragmentInfo(fileName);
    }


    /**
     * 获取片段设计构造表单
     * @param fileName
     * @return
     */
    @GetMapping("getFragmentFrom")
    public Map<String,Object> getFragmentDesign(@NotBlank @RequestParam String fileName){
        return  service.getFragmentFrom(fileName);
    }

    @PostMapping(value="createFile")
    public void createFile(@Validated(value = {ValidGroup1.class}) @RequestBody FragmentModelDto v){
        service.createFile(v);
    }

    @PostMapping("updateFragment")
    public  void updateFragment(@RequestBody FragmentModelDto v){
         service.updateFragment(v);
    }

    @GetMapping("getPartImport")
    public ApiResult getPartImport(@RequestParam String title){
       return service.getPartImport(title);
    }

}
