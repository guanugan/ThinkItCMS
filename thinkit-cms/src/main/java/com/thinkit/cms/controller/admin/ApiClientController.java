package com.thinkit.cms.controller.admin;
import com.thinkit.cms.api.admin.ApiClientService;
import com.thinkit.cms.dto.admin.ApiClientDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.Checker;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-05-05
 */
@Validated
@RestController
@RequestMapping("apiClient")
public class ApiClientController extends BaseController<ApiClientService> {

    @Logs(module = LogModule.ADMIN,operation = "给终端分配接口权限")
    @RequestMapping("/assignResource")
    public ApiResult assignMenu(@RequestBody ApiClientDto apiSourceClientDto) {
        if (Checker.BeEmpty(apiSourceClientDto.getOnCheckKeys()) || Checker.BeBlank(apiSourceClientDto.getClientId())) {
            return ApiResult.result("参数异常!", -1);
        }
        boolean f = service.assignResource(apiSourceClientDto);
        if (f) {
            return ApiResult.result();
        } else
            return ApiResult.result("操作失败", -1);
    }

}
