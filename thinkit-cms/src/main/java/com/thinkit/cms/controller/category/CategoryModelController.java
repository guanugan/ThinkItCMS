package com.thinkit.cms.controller.category;

import com.thinkit.cms.api.category.CategoryModelService;
import com.thinkit.cms.dto.category.CategoryModelDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 分类扩展模型 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-08
 */
@Validated
@RestController
@RequestMapping("categoryModel")
public class CategoryModelController extends BaseController<CategoryModelService> {


    @GetMapping("getByPk")
    public CategoryModelDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @SiteMark
    @PostMapping(value="save")
    public void save(@Validated @RequestBody CategoryModelDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody CategoryModelDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.delete(id);
    }

    @SiteMark
    @PostMapping("loadModels")
    public  List<CategoryModelDto> loadModels(@RequestBody CategoryModelDto v){
        return service.loadModels(v);
    }

    @SiteMark
    @PostMapping("page")
    public PageDto<CategoryModelDto> listPage(@RequestBody PageDto<CategoryModelDto> pageDto){
        pageDto.getDto().condition().orderByDesc("sort").like("category_model_name",pageDto.getDto().getCategoryModelName());
        return service.listPage(pageDto);
    }

}
