package com.thinkit.cms.controller.category;

import com.thinkit.cms.api.category.CategoryModelRelationService;
import com.thinkit.cms.dto.category.CategoryModelRelationDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.ValidList;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 分类选择模型时关系表 该模型值 tk_model 表 不是 分类模型表 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-08-10
 */
@Validated
@RestController
@RequestMapping("categoryModelRelation")
public class CategoryModelRelationController extends BaseController<CategoryModelRelationService> {


    @PostMapping(value="saveBatch")
    public void saveBatch(@Validated @RequestBody ValidList<CategoryModelRelationDto> vs){
        service.saveBatch(vs);
    }


}
