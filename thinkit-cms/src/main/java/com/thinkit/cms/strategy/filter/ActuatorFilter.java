package com.thinkit.cms.strategy.filter;
import com.thinkit.cms.strategy.checker.ActuatorCheck;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

public abstract class ActuatorFilter extends QuartzJobBean implements Filter {


    public void doFilter(Object params, FilterChain chain){

    }


    public void setChecker(ActuatorCheck actuatorCheck){

    }

    public ActuatorCheck getChecker(){
       return null;
    }

    public Object getParam(){
        return null;
    }

    public String triggerTime(){
        return null;
    }


    @Override
    protected void executeInternal(JobExecutionContext context)  {

    }
}
