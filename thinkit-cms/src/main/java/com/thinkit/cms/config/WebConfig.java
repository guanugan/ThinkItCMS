package com.thinkit.cms.config;
import com.thinkit.core.constant.Constants;
import com.thinkit.utils.properties.PermitAllUrlProperties;
import com.thinkit.utils.properties.ThinkItProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
public class WebConfig implements WebMvcConfigurer {


    @Autowired
    ThinkItProperties thinkItProperties;

    @Autowired
    AuthInterceptor authInterceptor;

    @Autowired
    PermitAllUrlProperties permitAllUrlProperties;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {//添加资源全局拦截器
        registry.addResourceHandler(Constants.localtionUploadPattern+"**").
        addResourceLocations("file:///"+thinkItProperties.getSourcePath()+Constants.localtionUploadPattern);

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> ignores =permitAllUrlProperties.ignores();
        // 自定义拦截器，添加拦截路径和排除拦截路径
        registry.addInterceptor(authInterceptor).addPathPatterns("/**").excludePathPatterns(ignores);
    }

}
