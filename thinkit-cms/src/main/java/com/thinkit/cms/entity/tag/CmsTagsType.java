package com.thinkit.cms.entity.tag;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 标签类型
 * </p>
 *
 * @author LG
 * @since 2020-01-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_tag_type")
public class CmsTagsType extends BaseModel {

private static final long serialVersionUID = 1L;
    /**
     * 名称
     */

        @TableField("name")
        private String name;

    /**
     * 标签数
     */

        @TableField("count")
        private Integer count;

        @TableField("site_id")
        private String siteId;


}
