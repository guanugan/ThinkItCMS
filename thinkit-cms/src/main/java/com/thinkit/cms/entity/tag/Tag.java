package com.thinkit.cms.entity.tag;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 标签
 * </p>
 *
 * @author lg
 * @since 2020-08-20
 */
@Data
@Accessors(chain = true)
@TableName("tk_tag")
public class Tag extends BaseModel {


        /**
         * 名称
         */
        @TableField("name")
        private String name;


        /**
         * 分类ID
         */
        @TableField("sort")
        private Integer sort;


        /**
         * 分类ID
         */
        @TableField("type_id")
        private String typeId;


        @TableField("site_id")
        private String siteId;
}
