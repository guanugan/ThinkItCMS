package com.thinkit.cms.entity.category;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分类扩展模型
 * </p>
 *
 * @author lg
 * @since 2020-08-08
 */
@Data
@Accessors(chain = true)
@TableName("tk_category_model")
public class CategoryModel extends BaseModel {


    /**
     * 分类模型扩展名称
     */

    @TableField("category_model_name")
    private String categoryModelName;

    /**
     * 站点ID
     */

    @TableField("site_id")
    private String siteId;


    /**
     * 扩展字段(json 保存)
     */

    @TableField("extend_field_list")
    private String extendFieldList;


    /**
     * 排序
     */

    @TableField("sort")
    private Integer sort;


    /**
     * 创建用户id
     */


    /**
     * 修改人id
     */


    /**
     * 创建时间
     */


    /**
     * 修改时间
     */


}
