package com.thinkit.cms.entity.content;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * <p>
 * 内容推荐
 * </p>
 *
 * @author lg
 * @since 2020-09-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_content_related")
public class ContentRelated extends BaseModel {


    /**
     * 当前内容id
     */

        @TableField("content_id")
        private String contentId;


    /**
     * 推荐的内容ID
     */

        @TableField("related_cid")
        private String relatedCid;


    /**
     * 排序
     */

        @TableField("sort")
        private Integer sort;


}
