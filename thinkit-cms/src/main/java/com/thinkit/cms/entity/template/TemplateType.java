package com.thinkit.cms.entity.template;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 模板分类
 * </p>
 *
 * @author lg
 * @since 2020-07-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_template_type")
public class TemplateType extends BaseModel {

    private static final long serialVersionUID = 1L;


    @TableField("name")
    private String name;


    @TableField("code")
    private String code;

    @TableField("parent_id")
    private String parentId;


    /**
     * 分类状态 0：显示 1：隐藏
     */

    @TableField("status")
    private Integer status;


}
