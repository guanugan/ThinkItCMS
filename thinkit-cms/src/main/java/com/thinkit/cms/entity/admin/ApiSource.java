package com.thinkit.cms.entity.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_api_source")
public class ApiSource extends BaseModel {



        @TableField("parent_id")
        private String parentId;


    /**
     * 0:模块 1：接口
     */

        @TableField("api_type")
        private Integer apiType;


    /**
     * 接口名称
     */

        @TableField("api_name")
        private String apiName;


    /**
     * 接口地址
     */

        @TableField("api_url")
        private String apiUrl;


    /**
     * 接口标识
     */

        @TableField("api_perm")
        private String apiPerm;


    /**
     * 接口描述
     */

        @TableField("api_desc")
        private String apiDesc;


    /**
     * 接口所属模块
     */

        @TableField("api_module")
        private String apiModule;



        /**
         * 排序
         */

        @TableField("api_order_num")
        private Integer apiOrderNum;


}
