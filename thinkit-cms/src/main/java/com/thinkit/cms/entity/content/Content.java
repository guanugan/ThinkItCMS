package com.thinkit.cms.entity.content;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 内容
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Data
@Accessors(chain = true)
@TableName("tk_content")
public class Content extends BaseModel {


    /**
     * 站点ID
     */

    @TableField("site_id")
    private String siteId;


    /**
     * 标题
     */

    @TableField("title")
    private String title;


    /**
     * 副标题
     */

    @TableField("sub_title")
    private String subTitle;


    /**
     * 审核用户
     */

    @TableField("approve_user_id")
    private String approveUserId;


    @TableField("approve_user_name")
    private String approveUserName;


    /**
     * 分类
     */

    @TableField("category_id")
    private String categoryId;


    /**
     * 模型
     */

    @TableField("model_id")
    private String modelId;


    /**
     * 是否转载
     */

    @TableField("copied")
    private Boolean copied;


    /**
     * 作者
     */

    @TableField("author")
    private String author;


    /**
     * 编辑
     */

    @TableField("editor")
    private String editor;


    /**
     * 是否置顶
     */

    @TableField("istop")
    private Boolean istop;


    /**
     * 是否推荐
     */

    @TableField("isrecomd")
    private Boolean isrecomd;


    /**
     * 是否头条
     */

    @TableField("isheadline")
    private Boolean isheadline;


    /**
     * 外链
     */

    @TableField("only_url")
    private Boolean onlyUrl;


    /**
     * 拥有附件列表
     */
    @TableField("has_files")
    private Boolean hasFiles;

    @TableField("has_tags")
    private Boolean hasTags;


    /**
     * 是否有推荐内容
     */

    @TableField("has_related")
    private Integer hasRelated;


    @TableField("p_category_id")
    private String pCategoryId;




    /**
     * 地址
     */

    @TableField("url")
    private String url;


    /**
     * 简介
     */

    @TableField("description")
    private String description;


    /**
     * 标签
     */

    @TableField("tag_ids")
    private String tagIds;


    /**
     * 置顶标签
     */

    @TableField("top_tag")
    private String topTag;


    /**
     * 封面
     */

    @TableField("cover")
    private String cover;


    /**
     * 评论数
     */

    @TableField("comments")
    private Integer comments;


    /**
     * 点击数
     */

    @TableField("clicks")
    private Integer clicks;


    /**
     * 日期生成规则
     */

    @TableField("path_rule")
    private String pathRule;


    /**
     * 点赞数
     */

    @TableField("give_likes")
    private Integer giveLikes;


    /**
     * 发布日期
     */

    @TableField("publish_date")
    private Date publishDate;


    /**
     * 发表用户ID
     */

    @TableField("publish_user_id")
    private String publishUserId;


    /**
     * 定时发布日期
     */

    @TableField("job_date")
    private Date jobDate;


    /**
     * 审核日期(c)
     */

    @TableField("approve_date")
    private Date approveDate;


    /**
     * 顺序
     */

    @TableField("sort")
    private Integer sort;


    /**
     * 状态：0、草稿 1、已发布 2、删除
     */

    @TableField("status")
    private String status;


    @TableField("create_name")
    private String createName;


    /**
     * 修改人名称
     */

    @TableField("modified_name")
    private String modifiedName;


}
