package com.thinkit.cms.dto.site;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 站点模型表
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SiteTemplateDto extends BaseDto {

        private static final long serialVersionUID = 1L;

        /**
        * 站点id
        */
        @NotBlank(message = "站点不能为空")
        private String siteId;


        /**
        * 模型id
        */
        @NotBlank(message = "模板不能为空")
        private String templateId;


        /**
        * 模板路径
        */
        private String templateFolderPath;


        /**
        * 0:未启用 1：已启用
        */
        private Integer startUsing;

}
