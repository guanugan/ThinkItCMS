package com.thinkit.cms.dto.content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 内容附件
 * </p>
 *
 * @author lg
 * @since 2020-08-19
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentAttachDto extends BaseDto {

    private static final long serialVersionUID = 1L;

        /**
        * 内容
        */
        private String contentId;


        /**
        * 关联资源表
        */
        private String fileUid;


        private String url;


        private String name;


        /**
        * 下载数
        */
        private Integer downs;


        /**
        * 排序
        */
        private Integer sort;


        /**
        * json 字段
        */
        private String data;











}
