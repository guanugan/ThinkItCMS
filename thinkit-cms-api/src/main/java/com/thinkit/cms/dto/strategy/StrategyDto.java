package com.thinkit.cms.dto.strategy;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lg
 * @since 2021-05-12
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StrategyDto extends BaseDto {

        /**
        * 动作编码
        */
        private String actionCode;


        /**
        * 动作名称
        */
        private String actionName;


        /**
        * 执行器编码
        */
        private String actuatorCode;


        /**
        * 执行器名称
        */
        private String actuatorName;


        /**
        * 栏目ID
        */
        private String categoryId;


        /**
        * 执行时间
        */
        private String triggerTime;


        /**
        * 顺序
        */
        private Integer orderNum;



        private Boolean isStart = false;
}
