package com.thinkit.cms.dto.admin;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 菜单管理
 * </p>
 *
 * @author dl
 * @since 2018-03-21
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuDto extends BaseDto {
    public MenuDto (){

    }
    public MenuDto (MetaDto metaDto){
        this.meta = metaDto;
    }

    /**
     * 父菜单ID，一级菜单为0
     */
    private String parentId;

    /**
     * 父菜单ID，一级菜单为0
     */
    private String parentName;

    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空")
    private String name;

    /**
     * 菜单URL
     */
    private String url;

    /**
     * 授权(多个用逗号分隔，如：user:list,user:create)
     */
    @NotBlank(message = "权限标识不能为空")
    private String perms;

    /**
     * 类型   0：目录   1：菜单   2：按钮
     */
    @NotNull(message = "菜单类型不能为空")
    private Integer type;

    /**
     * 类型   0：目录   1：菜单   2：按钮
     */
    private Integer typeName;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空")
    private Integer orderNum;

    /**
     * 是否隐藏
     */
    private Boolean hidden;


    /**
     * 是否缓存页面
     */
    private Boolean keepAlive;


    /**
     * 路由组件名称
     */
    private String component;

    /**
     * 重定向
     */
    private String redirect;

    /**
     * 路由地址
     */
    private String path;

    private MetaDto meta;

    private String key;
}
