package com.thinkit.cms.dto.tag;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 标签
 * </p>
 *
 * @author lg
 * @since 2020-08-20
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TagDto extends BaseDto {

        /**
        * 名称
        */
        private String name;


        /**
        * 分类ID
        */
        private Integer sort;

        /**
         * 分类ID
         */
        private String typeId;


        private String typeName;
}
