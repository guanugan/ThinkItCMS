package com.thinkit.cms.dto.site;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 用户站点设置表
 * </p>
 *
 * @author LG
 * @since 2020-09-20
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSiteSettingDto extends BaseDto {

        private String userId;


        private String siteId;


        private String orgId;


        private Boolean isDefault;

}
