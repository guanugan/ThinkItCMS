package com.thinkit.cms.dto.tag;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 标签类型
 * </p>
 *
 * @author LG
 * @since 2020-01-31
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CmsTagsTypeDto extends BaseDto {

       private static final long serialVersionUID = 1L;

        /**
        * 名称
        */
        private String name;


        /**
        * 标签数
        */
        private Integer count;



        List<String> tagIds;

}
