package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * <p>
 * 分类
 * </p>
 *
 * @author lg
 * @since 2020-08-07
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryDto extends BaseDto {

    private static final long serialVersionUID = 1L;

        /**
        * 名称
        */
        @NotBlank(message = "栏目名称不能为空",groups = {ValidGroup1.class})
        private String name;


        /**
         * 父栏目名称
         */
        private String parentName;

        /**
        * 父分类ID
        */
        @NotBlank(message = "父栏目不能为空",groups = {ValidGroup1.class})
        private String parentId;


        /**
        * 站点ID
        */
        private String siteId;


        /**
        * 编码
        */
        @NotBlank(message = "编码不能为空",groups = {ValidGroup1.class})
        private String code;


        /**
        * 生成路径规则
        */
        @NotBlank(message = "路径规则不能为空",groups = {ValidGroup1.class})
        private String pathRule;


        /**
        * 分类首页路径
        */
        private String path;


        /**
        * 是否只外链 只是外链栏目直接访问
        */
        private Boolean onlyUrl;


        /**
        * 外链跳转地址
        */
        private String remoteUrl;


        /**
        * 每页数据条数
        */
        @NotNull(message = "数据条数不能为空",groups = {ValidGroup1.class})
        private Integer pageSize;


        /**
        * 允许投稿
        */
        @NotNull(message = "投稿配置不能为空",groups = {ValidGroup1.class})
        private Boolean allowContribute;


        /**
        * 顺序
        */
        private Integer sort;


        /**
        * 是否在首页隐藏
        */
        @NotNull(message = "隐藏配置不能为空",groups = {ValidGroup1.class})
        private Boolean hidden;


        /**
        * 扩展模型ID
        */
        private String categoryModelId;


        /**
        * 是否是单页
        */
        @NotNull(message = "单页配置不能为空",groups = {ValidGroup1.class})
        private Boolean singlePage;


        /**
        * 该栏目多少秒后自动排序生成
        */
        private Integer topPages;

        private String templatePath;

        /**
        * 是否包含字内容
        */
        private Boolean containChild;

        /**
         * -----attr-------------
         */
        private String title;

        private String keywords;

        private String description;

        private String data;

        private Map<String,Object> params;
}
