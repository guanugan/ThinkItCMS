package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分类-模板配置表
 * </p>
 *
 * @author LG
 * @since 2020-08-12
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryTemplateDto extends BaseDto {

    private static final long serialVersionUID = 1L;

        /**
        * 分类ID
        */
        private String categoryId;


        /**
        * 站点ID
        */
        private String siteId;


        /**
        * 模板ID
        */
        private String templateId;


        /**
        * 模板列表页路径
        */
        private String templatePath;

}
