package com.thinkit.cms.dto.admin;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Permission {

    private String url;

    private String perms;
}
