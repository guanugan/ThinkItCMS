package com.thinkit.cms.dto.admin;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MetaDto {

    private String icon;

    private String title;

    private Boolean hidden;

    private Boolean keepAlive;

}
