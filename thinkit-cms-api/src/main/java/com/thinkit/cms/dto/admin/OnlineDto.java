package com.thinkit.cms.dto.admin;

import com.thinkit.utils.model.BaseDto;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author dl
 * @since 2018-03-28
 */
@Data
public class OnlineDto extends BaseDto {

    private static final long serialVersionUID = 1L;

    private String tableName;

    private String tableNote;

    private String engine;

    private String position;

    private String prefix;
    
    private String author;
    
    private int fileOverride;
    
    private int open;
    
    private int resultmap;
    
    private int columnList;

    private int enableCache;
    
    private String location;

    //模块名称
    private String modelName;

    private String  superControllerClass;
    private String  superServiceClass	;
    private String  superServiceImplClass	;
    private String  superEntityClass;
    private String  superMapperClass;


    private String  basePack	;
    private String  controllerPack	;
    private String  servicePack	;
    private String  serviceImplPack	;
    private String  mapperPack	;
    private String  entityPack	;


}
