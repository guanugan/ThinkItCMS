package com.thinkit.cms.api.admin;

import com.thinkit.cms.dto.admin.ClientsDto;
import com.thinkit.core.base.BaseService;

import java.util.List;

/**
 * <p>
 * 终端信息表 服务类
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
public interface ClientsService extends BaseService<ClientsDto> {

    List<ClientsDto> listClients();

    /**
     * 更新客户端秘钥
     * @param clientDto
     */
    void updateSecretByClientId(ClientsDto clientDto);
}