package com.thinkit.cms.api.admin;

import com.thinkit.cms.dto.admin.ApiClientDto;
import com.thinkit.core.base.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LG
 * @since 2020-05-05
 */
public interface ApiClientService extends BaseService<ApiClientDto> {


    List<String> selectResources(String clientId);

    /**
     * 分配资源
     * @param apiSourceClientDto
     * @return
     */
    boolean assignResource(ApiClientDto apiSourceClientDto);


    boolean deleteByClientId(String clientId);


    boolean deleteByResourceId(String resourceId);

    List<ApiClientDto> getsByApiId(String parentId);

}