package com.thinkit.cms.api.resource;

import com.thinkit.cms.dto.resource.SysFileGroupDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.ApiResult;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sysFileGroup
 * @since 2021-07-03
 */
public interface SysFileGroupService extends BaseService<SysFileGroupDto> {


    List<String> listGroupIds();

    List<SysFileGroupDto> listGroupName(SysFileGroupDto v);

    ApiResult addGroup(SysFileGroupDto v);

    ApiResult deleteGroup(String id);

    ApiResult updateFileGid(String fileId, String gid);

    void renameGroup(SysFileGroupDto v);
}