package com.thinkit.cms.api.site;
import com.thinkit.cms.dto.site.UserSiteSettingDto;
import com.thinkit.core.base.BaseService;
import java.util.List;

/**
 * <p>
 * 用户站点设置表 服务类
 * </p>
 *
 * @author LG
 * @since 2020-09-20
 */
public interface UserSiteSettingService extends BaseService<UserSiteSettingDto> {

    UserSiteSettingDto getBySiteId(String siteId);

    List<UserSiteSettingDto> getByUserId(String userId);

    void setDefaultSetting(String siteId);


    void saveSetting(String orgId, String userId, String siteId);
}