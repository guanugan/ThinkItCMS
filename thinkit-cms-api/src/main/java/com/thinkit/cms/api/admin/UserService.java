package com.thinkit.cms.api.admin;

import com.thinkit.cms.dto.admin.UserDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.ApiResult;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserService extends BaseService<UserDto> {
    /*
    根据用户名称查询用户
     */
	UserDto findUserByUsername(String username);

	boolean save(UserDto userDto);

    boolean deleteByUserId(String userId);

	boolean update(UserDto userDto);

    boolean deleteByIds(List<String> ids);

    boolean lockUsers(boolean justLock);
    /*
    获取用户个人信息以及角色权限
     */
    Map<String,Object> info();

    UserDto getById(String id);

    boolean batch(Integer type, List<String> ids);

    boolean updateUserInfo(UserDto userDto);

    List<UserDto> getUserByOrgId(String oid);

    void modifyPass(UserDto user);

    /**
     * 重置密码
     * @param id
     * @return
     */
    ApiResult resetPass(String id);

    /**
     * 查询用户角色标识
     * @param id
     * @return
     */
    Set<String> selectRoleSignByUserId(String id);

    List<String> getUserIdsByOrgId(String orgId);

     UserDto loadUserByUsername(String userAccount);

    ApiResult verifyCode();
}
