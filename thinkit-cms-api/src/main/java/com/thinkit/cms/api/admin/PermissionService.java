package com.thinkit.cms.api.admin;

import java.util.Set;

public interface PermissionService {

    /**
     * 根据请求url查询 对应的标识
     * @param url :请求连接
     * @param clientId 客户端标识
     * @return
     */
   String selectPermIdsByUrl(String url, String clientId);

    /**
     *
     * @param userId 会员ID
     * @param clientId 客户端标识
     * @return
     */
   Set<String> selectPermsByUid(String userId, String clientId);
}
