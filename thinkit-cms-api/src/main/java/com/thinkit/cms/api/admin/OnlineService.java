package com.thinkit.cms.api.admin;

import com.thinkit.cms.dto.admin.OnlineDto;
import com.thinkit.core.base.BaseService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dl
 * @since 2018-03-28
 */
public interface OnlineService extends BaseService<OnlineDto> {
	/** 
	* @Description: 初始化table
	* @param   
	* @return void 
	* @throws 
	*/ 
	void initTable();
	
	void down(OnlineDto online, HttpServletRequest request);

	void deleteByPk(Long id);

	void updateById(OnlineDto online);
}
