package com.thinkit.cms.api.resource;

import com.thinkit.cms.dto.resource.SysResourceDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.PageDto;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LG
 * @since 2019-11-11
 */
public interface SysResourceService extends BaseService<SysResourceDto> {


	String getfilePathById(String id);

    void deleteByFilePath(String filePath);

    PageDto<SysResourceDto> listGroupPage(PageDto<SysResourceDto> pageDto);

    void downFile(String id, HttpServletResponse response);
}