package com.thinkit.directive.render;
import com.thinkit.directive.emums.LangEnum;
import freemarker.template.TemplateModel;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *模板渲染 基类 用于包装 模板指令参数封装等
 */
public class RenderWrapper implements BaserRender{

     private Wrapper wrapper;

     protected Map<String, Object> map = new LinkedHashMap<>();

     public RenderWrapper(Wrapper wrapper){
          this.wrapper = wrapper;
     }


     @Override
     public void render() throws Exception {
          wrapper.construct(map);
     }

     @Override
     public String getString(String name) throws Exception {
          return wrapper.getParam(name, LangEnum.STRING);
     }

     @Override
     public String getString(String name, String defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.STRING);
     }

     @Override
     public Integer getInteger(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.INTEGER);
     }

     @Override
     public Integer getInteger(String name, Integer defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.INTEGER);
     }

     @Override
     public Long getLong(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.LONG);
     }

     @Override
     public Long getLong(String name, Long defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.LONG);
     }

     @Override
     public Short getShort(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.SHORT);
     }

     @Override
     public Short getShort(String name, Short defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.SHORT);
     }

     @Override
     public Double getDouble(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.DOUBLE);
     }

     @Override
     public Double getDouble(String name, Double defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.DOUBLE);
     }

     @Override
     public String[] getStringArray(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.STRING_ARRAY);
     }

     @Override
     public Long[] getLongArray(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.LONG_ARRAY);
     }

     @Override
     public Integer[] getIntegerArray(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.INTEGER_ARRAY);
     }

     @Override
     public Float getFloat(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.FLOAT);
     }

     @Override
     public Float getFloat(String name, Float defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.FLOAT);
     }

     @Override
     public Boolean getBoolean(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.BOOLEAN);
     }

     @Override
     public Boolean getBoolean(String name, Boolean defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal,LangEnum.BOOLEAN);
     }

     @Override
     public <T> T getBean(String name, Class<T> clz) throws Exception {
          return wrapper.getParam(name,LangEnum.BEAN);
     }

     @Override
     public TemplateModel getMap(String name) throws Exception {
          return wrapper.getParam(name,LangEnum.MAP);
     }

     @Override
     public BaserRender putAll(Map maps) {
          map.putAll(maps);
          return this;
     }

     @Override
     public BaserRender put(String key, Object value) {
          map.put(key,value);
          return this;
     }
}