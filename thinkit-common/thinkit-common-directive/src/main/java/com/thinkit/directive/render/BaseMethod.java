package com.thinkit.directive.render;

import com.thinkit.directive.emums.LangEnum;
import com.thinkit.directive.emums.MethodEnum;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 
 * BaseMethod FreeMarker自定义方法基类
 *
 */
@Data
public abstract class BaseMethod extends MethodWrapper implements TemplateMethodModelEx {


    public static String getString(int index, List<TemplateModel> arguments, LangEnum langEnum) throws TemplateModelException {
         return getParam(index,arguments,langEnum);
    }

    public static Short getShort(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static Integer getInteger(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static Boolean getBoolean(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static Date getDate(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static Long getLong(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static Double getDouble(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static String[] getStringArray(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static String[] getIntegerArray(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }


    public static String[] getLongArray(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public static TemplateHashModelEx getMap(int index, List<TemplateModel> arguments,LangEnum langEnum) throws TemplateModelException {
        return getParam(index,arguments,langEnum);
    }

    public abstract MethodEnum getName();
}
