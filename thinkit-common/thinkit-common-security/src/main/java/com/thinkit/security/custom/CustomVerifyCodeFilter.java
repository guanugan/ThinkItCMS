package com.thinkit.security.custom;

import cn.hutool.json.JSONUtil;
import com.thinkit.core.constant.Constants;
import com.thinkit.nosql.base.BaseRedisService;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class CustomVerifyCodeFilter implements Filter {

    @Autowired
    private BaseRedisService baseRedisService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String url = request.getRequestURI();
        if(login(url,request)){
            if(verifyCode(request,response)){
                chain.doFilter(request,response);
            }
            return ;
        }
        chain.doFilter(request,response);
    }


    private boolean verifyCode (HttpServletRequest request, HttpServletResponse response) throws IOException {
        String verifyCode = request.getParameter("verify_code");
        String uid = request.getParameter("uid");
        if(Checker.BeBlank(verifyCode) || Checker.BeBlank(uid)){
            writeMsg(response,HttpStatus.FORBIDDEN,uid,"请输入验证码!");
            return false;
        }else{
            Object code = baseRedisService.get(Constants.VerifyCode+uid);
            if(Checker.BeNull(code)){
                writeMsg(response,HttpStatus.FORBIDDEN,uid,"验证码不正确!");
                return false;
            }
            if(code.toString().toLowerCase().equals(verifyCode.toLowerCase())){
                baseRedisService.remove(Constants.VerifyCode+uid);
                return true;
            }else{
                writeMsg(response,HttpStatus.FORBIDDEN,uid,"验证码不正确!");
                return false;
            }
        }
    }

    private void writeMsg(HttpServletResponse response,HttpStatus httpStatus,String uid,String errMsg) throws IOException {
            baseRedisService.remove(Constants.VerifyCode+uid);
            response.setStatus(httpStatus.value());
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json; charset=utf-8");
            PrintWriter writer = response.getWriter();
            writer.write(JSONUtil.toJsonStr(ApiResult.result(httpStatus.value(),errMsg)));
    }

    private boolean login(String url, HttpServletRequest request){
        if("/oauth/token".equals(url)){ //授权验证
            String grantType = request.getParameter("grant_type");
            if(Checker.BeNotBlank(grantType) && "password".equals(grantType)){
                 return true;
            }
        }
        return false;
    }
}
