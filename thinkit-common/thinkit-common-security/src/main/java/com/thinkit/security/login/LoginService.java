package com.thinkit.security.login;

import com.thinkit.cms.dto.admin.MemberDto;
import com.thinkit.cms.dto.admin.UserDto;

public interface LoginService {

     UserDto loadUserByUsername(String userAccount);

     MemberDto loadMemByUsername(String userAccount);

}
