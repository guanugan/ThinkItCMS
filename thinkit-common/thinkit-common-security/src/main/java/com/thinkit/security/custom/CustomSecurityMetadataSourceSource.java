package com.thinkit.security.custom;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * @ClassName: CustomSecurityMetadataSource
 * @Author: LG 动态的权限验证，根据请求路径判断用户权限是否存在
 * @Date: 2019/4/30 11:38
 * @Version: 1.0
 **/

public class CustomSecurityMetadataSourceSource extends CustomAuthPermsSource implements FilterInvocationSecurityMetadataSource {

    public void setRequestMatcher(RequestMatcher requestMatcher){
        super.requestMatcher = requestMatcher;
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        FilterInvocation filterInvocation = ((FilterInvocation) object);
        HttpServletRequest request = filterInvocation.getHttpRequest();
        if(isAuthcUrl(request)){
           return returnAuthcUrl();
        }
        String url = request.getServletPath();
        return queryPermByUrl(url);
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }
}
