package com.thinkit.nosql.annotation;
import com.thinkit.core.constant.Constants;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheClear {

	Class<?>[] clas() default {};

	String method() default "";

	String key() default  "";

	String[] keys() default {};

	String value() default Constants.cacheNameClear;
}
