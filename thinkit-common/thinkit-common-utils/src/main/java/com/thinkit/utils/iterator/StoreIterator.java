package com.thinkit.utils.iterator;
import cn.hutool.json.JSONUtil;
import com.thinkit.utils.annotation.StoreMark;
import com.thinkit.utils.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class StoreIterator implements ThinkIterator {

    public BeanPropertyBox beanPropertyBox;

    private int i=0;

    private static final String extend ="extend_field_";

    Map<String,Object> param=new LinkedHashMap<>(16);

    public StoreIterator(BeanPropertyBox beanPropertyBox){
        this.beanPropertyBox=beanPropertyBox;
    }

    @Override
    public boolean hasNext() {
        return i<beanPropertyBox.size();
    }

    @Override
    public Object next() {
        param.clear();
        Object obj=beanPropertyBox.getBean();
        Field field= beanPropertyBox.getField(i);
        field.setAccessible(true);
        StoreMark solrMark=field.getAnnotation(StoreMark.class);
        if(Checker.BeNotNull(solrMark)){
            try {
                Object val = field.get(obj);
                if(Checker.BeNotNull(val)){
                    String name = Checker.BeNotBlank(solrMark.name()) ? solrMark.name():field.getName();
                    name= solrMark.expand()?extend+name:name;
                    if(val instanceof Map){
                        Map valMap=(Map) val;
                        Iterator<Map.Entry<String, Object>> it = valMap.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry<String, Object> data = it.next();
                            param.put(extend + data.getKey(), data.getValue());
                        }
                    }
                    else if(val instanceof List ){
                        List valList=(List) val;
                        if(Checker.BeNotEmpty(valList)){
                            param.put(name, JSONUtil.toJsonStr(valList));
                        }
                    }
                    else{
                        param.put(name, val);
                    }
                }
            }catch (IllegalAccessException e) {
                log.error(e.getMessage());
            }
        }
        i++;
        return param;
    }
}
