package com.thinkit.utils.iterator;

public interface IteratorFactory {

    ThinkIterator iterator(Class cls);
}
