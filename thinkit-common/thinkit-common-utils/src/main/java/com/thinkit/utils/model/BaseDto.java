package com.thinkit.utils.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thinkit.utils.annotation.StoreMark;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by JueYue on 2017/9/14.
 */
@Data
@Accessors(chain = true)
public class BaseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @StoreMark
    private String id;


    /**
     * 站点
     */
    @StoreMark
    private String siteId;

	/**
     * 创建人
     */
    private String createId;


    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    /**
     * 修改人
     */
    private String modifiedId;


    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @JsonIgnore
    private ConditionModel condition;

    @JsonIgnore
    public ConditionModel condition(){
        return ConditionModel.build(this);
    }

}
