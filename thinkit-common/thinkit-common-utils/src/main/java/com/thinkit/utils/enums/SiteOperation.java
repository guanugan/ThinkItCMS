package com.thinkit.utils.enums;

import lombok.Getter;

public enum SiteOperation {
    WRITE("插入或者更新"),
    READ("查询网站");
    @Getter
    private String code;
    SiteOperation(String code) {
        this.code = code;
    }
}
