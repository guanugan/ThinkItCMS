package com.thinkit.utils.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
@Data
public class SolrSearchModel extends BaseDto{

    private String keyWords;

    private String author;

    private String categoryId;

    private String content;

    private String cover;

    private String description;

    private String editor;

    @JsonFormat(pattern="yyyy-MM-dd")
    private String publishDate;

    private String code;

    private String domian;
}
