package com.thinkit.processor.resove;
import com.thinkit.core.constant.Channel;
import com.thinkit.core.handler.CustomException;
import com.thinkit.processor.channel.BaseChannelService;
import com.thinkit.processor.channel.ChannelThreadLocal;
import com.thinkit.processor.message.MessageModel;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class MessageResolveHandler extends ResolveHandlerAdapt{

    private MessageModel messageModel;

    private BaseChannelService channelService;

    private ThinkItProperties thinkItProperties;

    public MessageResolveHandler(MessageModel messageModel,ThinkItProperties thinkItProperties){
        super(messageModel.getMessage());
        this.messageModel = messageModel;
        this.thinkItProperties = thinkItProperties;
    }

    @Override
    public void run() {
        String methodName = getMethod();
        Object[] args = getArgs();
        MessageProxy messageProxy =new MessageProxy();
        Object instance =messageProxy.proxyObject(channelService);
        ChannelThreadLocal.set(Channel.SITE_ID,getSiteId());
        ChannelThreadLocal.set(Channel.USER_ID,getUserId());
        Method method;
        try {
            if(Checker.BeNotNull(args)){
                method = instance.getClass().getMethod(methodName, getClases(args));
            }else{
                method = instance.getClass().getMethod(methodName);
            }
            if(Checker.BeNotNull(method)){
                method.invoke(instance,args);
            }
        } catch (Exception e) {
            if(e instanceof NoSuchMethodException){
                log.error("method:"+methodName+" not fount:"+e.getMessage()+"找不到代理方法，请查看");
            }else if(e instanceof InvocationTargetException){
                log.error("====================方法执行异常请注意查看=======================");
                log.error("Target Method:"+methodName+" exception:"+e.getMessage());
            }
        }finally {
            ChannelThreadLocal.remove();
        }
    }

    private Class<?>[] getClases(Object[] args){
        List<Class<?>> classes =new ArrayList<>();
        for(Object arg: args){
            if(arg instanceof Map){
                classes.add(Map.class);
            }else if(arg instanceof List){
                classes.add(List.class);
            }else{
                classes.add(arg.getClass());
            }
        }
        Class<?>[] cz = new Class<?>[classes.size()];
        return classes.toArray(cz);
    }

    @Override
    public String channel() {
        return messageModel.getChannel();
    }

    @Override
    public void channelService(BaseChannelService channelService) {
        this.channelService = channelService;
    }
}
